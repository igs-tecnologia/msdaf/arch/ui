import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { LoaderComponent } from './components/loader/loader.component';
import { AppLayoutComponent } from './layouts/app-layout/app-layout.component';
import { AppLayoutModule } from './layouts/app-layout/app-layout.module';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { TemplatesLayoutComponent } from './layouts/templates-layout/templates-layout.component';
import { FormatCurrencyPipe } from './pipes/format-currency.pipe';

registerLocaleData(localePt);

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    AppLayoutModule      
  ],
  declarations: [
    AppComponent,
    AppLayoutComponent,
    TemplatesLayoutComponent,
    AuthLayoutComponent,    
    FormatCurrencyPipe,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    LoaderComponent,
     {provide: LOCALE_ID, useValue: "pt-BR"}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
