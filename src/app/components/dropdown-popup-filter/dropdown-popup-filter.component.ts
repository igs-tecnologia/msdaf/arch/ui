import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dropdown-popup-filter',
  templateUrl: './dropdown-popup-filter.component.html',
  styleUrls: ['./dropdown-popup-filter.component.scss']
})
export class DropdownPopupFilterComponent implements OnInit, AfterViewInit {

  @Input() type: any;
  @Input() header: any;
  @Output() msgEmit = new EventEmitter<Object>();
  iStarts: any;
  iEnds: any;
  adjSorting: any;

  constructor(private ngbConfig: NgbDropdownConfig) {
    this.ngbConfig.autoClose = 'outside';
    this.ngbConfig.placement = 'right-top';
   }

  ngOnInit() {
  }
  
  ngAfterViewInit() {
  }

  // intervalStarts(e) {
  //   console.log(e.srcElement.value);
  //   this.iStarts = e.srcElement.value;
  // }

  // intervalEnds(e) {
  //   console.log(e.srcElement.value);
  //   this.iEnds = e.srcElement.value;
  // }

  adjustSorting(e) {
    console.log(e.srcElement.value);
    this.adjSorting = e.srcElement.value;
  }

  update() {
    this.msgEmit.emit({
      type: this.type,
      header: this.header,
      intervalStarts: this.iStarts,
      intervalEnds: this.iEnds,
      order: this.adjSorting
    });
  }

}
