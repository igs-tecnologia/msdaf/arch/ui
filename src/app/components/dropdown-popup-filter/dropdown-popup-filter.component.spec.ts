import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownPopupFilterComponent } from './dropdown-popup-filter.component';

describe('DropdownPopupFilterComponent', () => {
  let component: DropdownPopupFilterComponent;
  let fixture: ComponentFixture<DropdownPopupFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownPopupFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownPopupFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
