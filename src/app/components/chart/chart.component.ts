import { Component, Input, OnInit, OnChanges } from '@angular/core';
import Chart from 'chart.js';

export interface ChartData {
  name: string;
  type: string;
  labels: Array<string>;
  dataType: string;
  data: any;
  scales: any;
  colors: {
    background: any;
    border: any;
  }
}

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})

export class ChartComponent implements OnInit {

  @Input('chartId') chartId: string;
  @Input('chartData') chartData: ChartData = {
    name: "Chart",
    type: "bar",
    labels: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"],
    dataType: 'Dados',
    data: [9, 7, 3, 5, 2, 10, 15, 16, 19, 3, 1, 9],
    scales: {},
    colors: {
      background: [],
      border: []
    }
  };

  title = 'DataMag';
  LineChart: Chart = {};

  constructor() {}
  
  ngOnInit() {

  }

  config() {
    this.LineChart = new Chart(document.getElementById(this.chartId), {
      type: this.chartData.type,
      data: {
        labels: this.chartData.labels,
        datasets: [{
          label: this.chartData.dataType,
          data: this.chartData.data,
          fill:false,
          lineTension:0.2,
          borderWidth: 1,
          backgroundColor: this.chartData.colors.background,
          borderColor: this.chartData.colors.border,
        }]
      }, 
      options: {
        title:{
          text: this.chartData.name,
          display:true
        },
        responsive: true,
        maintainAspectRatio: false,
        layout: {
          padding: 0
        },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            usePointStyle: true,
            padding: 10
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        }
        // stacked: true
      }
    });    
  }

  addDataset(dataset, labels) {
    this.LineChart.data.labels = labels;
    this.LineChart.data.datasets = dataset;
    this.LineChart.update();
  }
}
