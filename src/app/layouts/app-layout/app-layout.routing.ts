import { Routes } from '@angular/router';
import { MaterialComponent } from '../../pages/predictive/material/material.component';
import { MaterialDetailComponent } from '../../pages/predictive/material/material-detail/material-detail.component';
import { DashboardComponent } from '../../pages/budget/dashboard/dashboard.component';
import { AnalyticPage } from '../../pages/budget/analytic/analytic-page.component';
import { AgreementDetailsComponent } from '../../pages/budget/analytic/details/agreement-details/agreement-details.component';
import { MedicationDetailsComponent } from '../../pages/budget/analytic/details/medication-details/medication-details.component';
import { NotificationComponent } from '../../pages/popular-pharmacy/notification/notification/notification.component';
import { MagStartComponent } from '../../pages/budget/mag-start/mag-start.component';
import { ElucidationComponent } from '../../pages/popular-pharmacy/notification/elucidation/elucidation.component';
import { ProcessesComponent } from '../../pages/popular-pharmacy/processes/processes.component';
import { FormElucidationComponent } from '../../pages/popular-pharmacy/notification/form-elucidation/form-elucidation.component';
import { OccurrenceTypesComponent } from '../../pages/popular-pharmacy/notification/occurrence-types/occurrence-types.component';
import { MulctComponent } from '../../pages/popular-pharmacy/mulct/mulct.component';
import { SProcessesComponent } from '../../pages/reports/processes/processes.component';
import { PloaComponent } from '../../pages/budget/ploa/ploa.component';
import { GeoDataComponent } from '../../pages/geodata/geodata.component';
import { StockComponent } from '../../pages/stock/stock.component';


export const AppLayoutRoutes: Routes = [
  // Overview paths
  { path: '', component: MagStartComponent },
  { path: 'caf', component: MagStartComponent },

  //=====================================================================================================
  // Budget paths (Orçamento)
  { path: 'dashboard', component: DashboardComponent },
  
  
  //=====================================================================================================
  //Analytic paths (Visão Analítica)
  { path: 'analytic/medication/:coord', component: MedicationDetailsComponent},
  
  //-----------------------------------------------------------------------------------------------------
  { path: 'budget/ploa', component: PloaComponent },
  { path: 'budget/:coord', component: AnalyticPage },
  { path: 'budget/:coord/:view', component: AnalyticPage },
  { path: 'budget/:coord/:view/:aggreg', component: AnalyticPage },
  { path: 'budget/:coord/:view/:aggreg/:detail', component: AnalyticPage },
  { path: 'budget/:coord/:view/:aggreg/:detail/medication', component: MedicationDetailsComponent },
  { path: 'budget/:coord/:view/:aggreg/:detail/:subview', component: AnalyticPage },

  //=====================================================================================================
  // Medicines paths
  { path: 'medicines', component: AnalyticPage },
  // { path: 'medicines/:coord/:view/:aggreg/:detail', component: MedicationDetailsComponent },
  { path: 'medicines/:detail', component: MedicationDetailsComponent },

  //=====================================================================================================
  // Transfers paths
  { path: 'transfers', component: AnalyticPage },

  //=====================================================================================================
  // Automations
  { path: 'automation/notification', component: ElucidationComponent },
  { path: 'automation/notification/form', component: FormElucidationComponent },
  { path: 'automation/notification/form/:id', component: FormElucidationComponent },
  { path: 'automation/notification/occurrences-types', component: OccurrenceTypesComponent },
  { path: 'automation/mulct', component: MulctComponent },
  { path: 'automation/processes', component: ProcessesComponent },
  { path: 'automation/material', component:MaterialComponent },
  { path: 'automation/material/:id', component:MaterialDetailComponent },

  //=====================================================================================================
  // Researchs and Publications paths
  { path: 'researchs', component: SProcessesComponent },

  // ===================================================================================================
  // new screens in devel to be added soon.
  { path: 'screens/agreement-details', component: AgreementDetailsComponent },

  { path: 'dashboard/:detail', component: MedicationDetailsComponent },

  { path: 'features/geodata', component: GeoDataComponent },
  { path: 'features/stock', component: StockComponent },

];
