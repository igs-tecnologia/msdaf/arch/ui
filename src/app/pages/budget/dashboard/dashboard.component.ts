import { Component, OnInit, ViewChild } from '@angular/core';
import { AnalyticService } from '../../../services/analytic/analytic.service';
import { ChartData, ChartComponent } from '../../../components/chart/chart.component';

import {
  chartOptions,
  parseOptions,
  sinproc_json,
  sinproc_table_json,
  sinproc_informados_json,
  chartExample1,
  chartPercentData,
} from "../../../variables/charts";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild('chartRisk') chartRisk: ChartComponent;
  @ViewChild('chartBudget') chartBudget: ChartComponent;
  @ViewChild('chartPLOA') chartPLOA: ChartComponent;

  pageData = [];

  gRisk: any;
  gCurrentData: any;
  gCommittedData: any;
  gHistoryData: any;
  gEvolutionData: any;

  sinprocData: any;
  sinprocInformedData: any;

  constructor(
    private analyticService: AnalyticService
  ) { }

  ngOnInit() {

    this.sinprocData = sinproc_table_json;
    this.sinprocInformedData = sinproc_informados_json;
    
    this.analyticService.getDashGraph()
    .then((data: any) => {
      this.pageData = data;      
      
      this.gCurrentData = JSON.parse(JSON.stringify(chartExample1.data));
      
      this.gCurrentData.datasets[0].data = Object.values(this.pageData[0]).map((e: any) => (Object.values(e)[1]));
      this.gCurrentData.datasets[1].data = Object.values(this.pageData[0]).map((e: any) => (Object.values(e)[2]));
      this.gCurrentData.datasets[2].data = Object.values(this.pageData[0]).map((e: any) => (Object.values(e)[3]));
      this.gCurrentData.datasets[3].data = Object.values(this.pageData[0]).map((e: any) => (Object.values(e)[4]));
      this.gCurrentData.datasets[4].data = Object.values(this.pageData[0]).map((e: any) => (Object.values(e)[5]));
      this.gCurrentData.datasets[5].data = Object.values(this.pageData[0]).map((e: any) => (Object.values(e)[6]));

      this.gCommittedData = JSON.parse(JSON.stringify(chartExample1.data));
      this.gCommittedData.datasets = this.gCommittedData.datasets.slice(1,2);
      this.gCommittedData.datasets[0].data = Object.values(this.pageData[1]).map((e: any) => (Object.values(e)[1]));
      
      //---------------------------------------------------------------------------------------
      let gRiskConfig = {
        name : "",
        data : this.gCommittedData.datasets[0].data,
        dataType : this.gCommittedData.datasets[0].label,
        type : "pie",
        labels : this.gCommittedData.labels,
        scales : chartExample1.options.scales,
        colors:{
          background : chartExample1.data.datasets[0].backgroundColor,
          border : chartExample1.data.datasets[0].borderColor
        } 
      }

      this.chartRisk.chartData = gRiskConfig;
      this.chartRisk.config();
      this.chartRisk.addDataset([{
        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
        data: this.prepareChartRisk()
      }],["R0","R1","R2","R3","R4"]);

      //---------------------------------------------------------------------------------------
      let gCurrentDataConfig = {
        name: "",
        data: this.gCurrentData.datasets[0].data,
        dataType: this.gCommittedData.datasets[0].label,
        type: "bar",
        labels: this.gCommittedData.labels,
        scales: chartExample1.options.scales,
        colors: {
          background: chartExample1.data.datasets[0].backgroundColor,
          border: chartExample1.data.datasets[0].borderColor
        }
      }

      this.chartBudget.chartData = gCurrentDataConfig;
      this.chartBudget.config();
      this.chartBudget.addDataset(this.gCurrentData.datasets, this.gCurrentData.labels);

      console.log(this.gCurrentData.datasets);
      
      //---------------------------------------------------------------------------------------
      let gPLOAData = {
        name: "",
        data: this.gCommittedData.datasets[0].data,
        dataType: this.gCommittedData.datasets[0].label,

        type: "pie",
        labels: this.gCommittedData.labels,
        scales: chartPercentData.options.scales,
        colors:{
          background: chartPercentData.data.datasets[0].backgroundColor,
          border: chartPercentData.data.datasets[0].borderColor
        }
      }

      this.chartPLOA.chartData = gPLOAData;
      this.chartPLOA.config();
      this.chartPLOA.addDataset([{
        backgroundColor: ["#3e95cd","#c45850"],
        data: this.prepareChartPLOA()
      }],["Informado","Não Informado"]);
    });
  }

  prepareChartRisk(){
    let countRisk = [0, 0, 0, 0, 0]; 
    sinproc_json.forEach((elem) => {
      if(elem.Risco == "R0") countRisk[0] += 1;
      if(elem.Risco == "R1") countRisk[1] += 1;
      if(elem.Risco == "R2") countRisk[2] += 1;
      if(elem.Risco == "R3") countRisk[3] += 1;
      if(elem.Risco == "R4") countRisk[4] += 1;  
    });
    return countRisk;
  }

  prepareChartPLOA() {
    let countInformed = [0, 0];
    sinproc_informados_json.forEach((elem) => {
      if(elem.Valor != "") countInformed[0] += 1;
      if(elem.Valor == "") countInformed[1] += 1;
    });
    return countInformed;
  }
}
