import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-geodata',
  templateUrl: './geodata.component.html',
  styleUrls: ['./geodata.component.scss']
})
export class GeoDataComponent implements OnInit {

  constructor(private elementRef:ElementRef) {};

  ngOnInit() {
    
  }


  ngAfterViewInit() {
    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = "http://173.249.55.36:8080/geoserver/openlayers3/ol.js";
    this.elementRef.nativeElement.appendChild(s);
  }

}
