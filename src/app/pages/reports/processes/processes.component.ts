import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-processes',
  templateUrl: './processes.component.html',
  styleUrls: ['./processes.component.scss']
})
export class SProcessesComponent implements OnInit {

  public processes = [];
  public pdf: String;

  constructor() {}

  ngOnInit() {
    this.processes = [
      { title: 'Elaborar Termo de Referência', url: 'https://drive.google.com/open?id=153Wk1Pm0ph1g1vO-SvzQ3-_Ryay0hq3J', status: 'Em ajuste', pdf: `http://${window.location.host}/static/assets/pdfs/Coord - Elaborar Termo de Referência v2.pdf` },
      { title: 'Monitorar Estoque_Calamidade Pública', url: 'https://drive.google.com/open?id=153Wk1Pm0ph1g1vO-SvzQ3-_Ryay0hq3J', status: 'Elaboração', pdf: `http://${window.location.host}/static/assets/pdfs/Coord - Monitorar Estoque_Calamidade Pública.pdf` },
      { title: 'Monitorar Estoque_Saúde da Mulher e Insulina', url: 'https://drive.google.com/open?id=153Wk1Pm0ph1g1vO-SvzQ3-_Ryay0hq3J', status: 'Elaboração', pdf: `http://${window.location.host}/static/assets/pdfs/Coord - Monitorar Estoque_Saúde da Mulher e Insulina.pdf` },
      { title: 'Planejar Programação_Saúde da Mulher e Insulina', url: 'https://drive.google.com/open?id=153Wk1Pm0ph1g1vO-SvzQ3-_Ryay0hq3J', status: 'Elaboração', pdf: `http://${window.location.host}/static/assets/pdfs/Coord - Planejar Programação_Saúde da Mulher e Insulina.pdf` },
      { title: 'Realizar Aquisição Centralizada por Pregão e IRP', url: 'https://drive.google.com/open?id=153Wk1Pm0ph1g1vO-SvzQ3-_Ryay0hq3J', status: 'Em ajuste', pdf: `http://${window.location.host}/static/assets/pdfs/Coord - Realizar Aquisição Centralizada por Pregão e IRP.pdf` },
      { title: 'Realizar Aquisição Centralizada v2', url: 'https://drive.google.com/open?id=153Wk1Pm0ph1g1vO-SvzQ3-_Ryay0hq3J', status: 'Em ajuste', pdf: `http://${window.location.host}/static/assets/pdfs/Coord - Realizar Aquisição Centralizada v2.pdf` },
      { title: 'Preparar ressarcimento mensal do PFPB [AS IS]', url: 'https://drive.google.com/open?id=0B1neC5Yco3-OdXMxYkRlNzMtRzVPbkpRSjlKZmRBNWxZckNn', status: 'concluído', pdf: `http://${window.location.host}/static/assets/pdfs/02_02 Ressarcimento.pdf` },
      { title: 'Solicitar Credenciamento no PFPB [TO BE]', url: 'https://drive.google.com/open?id=0B1neC5Yco3-OYlQxVHBSYTlZNi1uT0FSOTZNMGdGbFUxSG1z', status: 'concluído', pdf: `http://${window.location.host}/static/assets/pdfs/01_01 Credenciamento.pdf` },
      { title: 'Validar Dados do Cadastro da Farmácia [TO BE]', url: 'https://drive.google.com/open?id=0B1neC5Yco3-OaGdkVzhRYVF2Q1RCYUZsMHVYTkdGOGlSZHBB', status: 'concluído', pdf: `http://${window.location.host}/static/assets/pdfs/01_01_01 Validar Dados da Farmácia.pdf` },
      { title: 'Realizar teste em homologação [TO BE]', url: 'https://drive.google.com/open?id=0B1neC5Yco3-ORVlzOVdXSV9YM3lQclZRbGlsOHpQRk9SZWhB', status: 'concluído', pdf: `http://${window.location.host}/static/assets/pdfs/01_01_02 Teste em Homologação.pdf` },
      { title: 'Realizar publicação de credenciamento no DOU', url: '-', status: 'elaborando pop', pdf: `` },
      { title: 'Multar empresa credenciada no PFPB', url: '-', status: 'Em ajuste', pdf: `` },
      { title: 'Renovar Credenciamento de Empresa', url: '-', status: 'a fazer', pdf: `` },
      { title: 'Realizar monitoramento eletrônico das empresas credenciadas no PFPB', url: '-', status: 'a fazer', pdf: `` },
      { title: 'Apurar irregularidades ou indícios de fraude no PFPB', url: '-', status: 'a fazer', pdf: `` },
      /* { title: 'Receber denúncias para apuração relativas ao PFPB', url: '', status: 'a fazer', pdf: `` },
      { title: 'Requerer documentação impressa de empresas credenciadas no PFPB', url: '', status: 'a fazer', pdf: `` },
      { title: 'Realizar cobrança administrativa de farmácia credenciada no PFPB', url: '', status: 'a fazer', pdf: `` },
      { title: 'Descredenciar empresa por irregularidade no PFPB', url: '', status: 'a fazer', pdf: `` }, */
      { title: 'Relatório de dados', url: '-', status: 'Elaboração', pdf: `http://${window.location.host}/static/assets/pdfs/(pt)DataReport.pdf` },
      { title: 'Relatório CNES', url: '-', status: 'Elaboração', pdf: `http://${window.location.host}/static/assets/pdfs/CNES BD.pdf` },
      { title: 'Relatório IA', url: '-', status: 'Elaboração', pdf: `http://${window.location.host}/static/assets/pdfs/MLPMicroset.pdf` },
      { title: 'Relatório IA FarmPop', url: '-', status: 'Em ajuste', pdf: `http://${window.location.host}/static/assets/pdfs/exploratory_analisys.pdf` },
      { title: 'AF 4.0 - Caminhos', url: '-', status: 'Em ajuste', pdf: `http://${window.location.host}/static/assets/pdfs/AssistFarm40.pdf` },
    ];
  }
  setPdf(pdf) {
    setTimeout(()=> {
      this.pdf = pdf;
    }, 500);
  }
}
