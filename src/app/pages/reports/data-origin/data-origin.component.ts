import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'data-origin',
  templateUrl: './data-origin.component.html',
  styleUrls: ['./data-origin.component.scss']
})

export class DataOriginComponent implements OnInit {

  
  @Input()
  pdf: any;

  constructor() { }

  ngOnInit() {
    this.pdf = `http://${window.location.host}/static/assets/pdfs/fonte_dados_mag.pdf`;
  }
}
