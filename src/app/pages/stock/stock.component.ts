import { Component, OnInit } from '@angular/core';
import { StockService } from '../../services/stock/stock.service';''


@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {

  tableTitle: String = "Estoque";
  dataTable: Object;
  search: string;
  field: string;

  constructor(
    private stockService: StockService
  ) { }

  ngOnInit() {
    this.stockService.getStock(1, 10).then((pload: any) => {
      // console.log(pload);
      
      this.dataTable = pload;
    });
  }

  getStockPage(e: any){
    console.log(this.search);
    if (!this.search) {
      this.stockService.getStock(e.page, e.qtd).then((p: any) => {
        // console.log(p);
        this.dataTable = p;
      });
    } else {
      this.stockService.getStockSearch(e.page, e.qtd, this.field, this.search).then((p: any) => {
        // console.log(p);
        this.dataTable = p;
      });
    }
  }

  getStockSearch(e: any) {
    this.search = e.search;
    this.field = e.field;
    this.stockService.getStockSearch(e.page, e.qtd, e.field, e.search).then((p: any) => {
      // console.log(p);
      this.dataTable = p;
    });
  }
}
